import axios from 'axios'
import Swal from 'sweetalert2'

const api = axios.create({
    baseURL: 'http://127.0.0.1:8000/core/',
    timeout: 100000,
    headers: {
        'Content-Type': 'application/json'
    }
})

api.interceptors.request.use( (config) => {
    return config;
}, (error) => {
    console.log(error);
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
    });
    return Promise.reject(error);
});

// Add a response interceptor
api.interceptors.response.use((response) => {
    return response;
}, (error) => {
    console.log(error);
    Swal.fire({
        icon: 'error',
        title: 'Oops...',
        text: 'Something went wrong!',
    });
    return Promise.reject(error);
});

export default api;