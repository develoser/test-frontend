import Vue from 'vue'
import App from './App.vue'

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faCircleNotch, faPlusCircle, faTimesCircle } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import { BootstrapVue } from 'bootstrap-vue'

import { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { required, length, numeric } from 'vee-validate/dist/rules';

library.add(faSearch);
library.add(faTimesCircle);
library.add(faCircleNotch);
library.add(faPlusCircle);

Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.config.productionTip = false;

Vue.use(BootstrapVue);

extend('required', required);
extend('length', length);
extend('numeric', numeric);

Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);

new Vue({
  render: h => h(App),
}).$mount('#app');