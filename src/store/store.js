import Vue from 'vue';
import Vuex from 'vuex';
import api from '../plugins/axios';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    selectedMetaData: {},
    metadata: [],
    records: [],
    databaseSearch: false,
    searchLoading: false
  },
  mutations: {
    SET_SELECTED_METADATA(state, data) {
      state.selectedMetaData = data
    },
    SET_RECORDS(state, data) {
      state.records = data
    },
    SET_METADATA(state, data) {
      state.metadata = data
    },
    TOGGLE_DATABASE_SEARCH(state) {
      state.databaseSearch = !state.databaseSearch;
    },
    SET_SEARCH_LOADING(state, data) {
      state.searchLoading = data;
    },
    REMOVE_SELECTED_METADA(state, data) {
      state.metadata = state.metadata.filter(item => {
        return item.id !== data
      })
    }
  },
  actions: {
    getMetaData({ commit }, text) {
      commit('SET_SEARCH_LOADING', true);
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        api.get('meta/', {
          params: { search: text }
        }).then(response => {
          commit('SET_METADATA', response.data);
          commit('SET_SELECTED_METADATA', []);
          commit('SET_RECORDS', []);
          commit('SET_SEARCH_LOADING', false);
        })
      }, 1400)
    },
    getRecords({ commit }, text) {
      commit('SET_SEARCH_LOADING', true);
      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        api.get('records/', {
          params: { search: text }
        }).then(response => {
          commit('SET_RECORDS', response.data);
          commit('SET_SEARCH_LOADING', false);
        })
      }, 1400)
    },
    selectMetaData({ commit }, data) {
      commit('SET_SELECTED_METADATA', data);
      api.post("meta/search", {
        input_record: data
      }).then((response) => {
        console.log(response);
        commit('SET_RECORDS', response.data);
      })
    },
    createMetaData(context, data) {
      return api.post("records/", data)
    },
    setRecordOnMetaData({ commit, state }, data) {
      return api.post("meta/set", {
        input_record_id: state.selectedMetaData.id,
        record_id: data
      }).then((response) => {
        console.log(response);
        commit('REMOVE_SELECTED_METADA', state.selectedMetaData.id);
        commit('SET_SELECTED_METADATA', []);
        commit('SET_RECORDS', []);

      })
    }
  }
});

export default store